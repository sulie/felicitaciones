<?php

use Illuminate\Database\Seeder;
use App\Models\Cliente;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables([
            'clientes',
            //'users'
        ]);
        //self::seedCatalog();
        $this->command->info('Tabla clientes inicializada con datos!');
        // $this->call(UsersTableSeeder::class);
        factory(App\Models\Cliente::class)->times(20)->create();
        //factory(App\User::class)->times(20)->create();
    }
    /*
    public function seedCatalog() {
        DB::table('clientes')->delete();
        foreach($this->arrayClientes as $cliente ) {
            $c = new Cliente;
            $c->nombre = $cliente['nombre'];
            $c->imagen = $cliente['imagen'];
            $c->fecha_nacimiento = $cliente['fecha_nacimiento'];
            $c->correo = $cliente['correo'];
            $c->save();
        }
    }
    */
    //Borra los registros de las tablas
    public function truncateTables(array $tables) {
        foreach ($tables as $table) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table($table)->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }
}
