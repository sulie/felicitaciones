<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cliente;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'fecha_nacimiento'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'correo'=>$faker->safeEmail(),
        //'imagen'=>$faker->imageUrl($width=200, $height=150, 'cats', true, 'Faker')
        'imagen'=>'placeholder.png'
    ];
});
