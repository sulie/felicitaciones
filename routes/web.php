<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', function () {
    return '';
});
*/
Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {

    Route::get('catalog', 'CatalogController@getIndex');

    Route::get('catalog/show/{id}', 'CatalogController@getShow');
    
    Route::get('catalog/create', 'CatalogController@getCreate');

    Route::post('catalog/create', 'CatalogController@postCreate');
    
    Route::get('catalog/edit/{id}', 'CatalogController@getEdit');

    Route::put('catalog/edit/{id}', 'CatalogController@putEdit');

    Route::delete('catalog/delete/{id}', 'CatalogController@putDelete');
    
});

Route::get('/', 'HomeController@index')->middleware('language');

Route::view('auth/login', 'auth/login'); // Sin controlador

Route::get('sendbasicemail/{cliente}','MailController@basic_email');
Route::get('sendhtmlemail','MailController@html_email');
Route::get('sendattachmentemail','MailController@attachment_email');

// PRUEBAS

Route::resource('/prueba', 'PruebaController');
Route::get('/request', 'PruebaRequestController@index');


// FIN DE PRUEBAS
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
