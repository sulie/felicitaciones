@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Añadir cliente
         </div>
         <div class="card-body" style="padding:30px">

        @if ($errors->any())

        <div class="row justify-content-center">
            <div class="col-sm-7">
                <div class="alert alert-danger">
                    <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div> 

        @endif

            <form method="POST" enctype="multipart/form-data">

                @csrf

                <div class="form-group">
                <label for="title">Nombre</label>
                <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">
                </div>

                <div class="form-group">
                    <input type="file" name="foto">
                </div>

                <div class="form-group">
                    <input type="date" name="fecha" value="{{old('fecha')}}">
                </div>

                <div class="form-group">
                    <input type="email" name="email" value="{{old('email')}}">
                </div>
            
                <div class="form-group text-center">
                <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                    Añadir cliente
                </button>
                </div>

            </form>

         </div>
      </div>
   </div>
</div>
@stop