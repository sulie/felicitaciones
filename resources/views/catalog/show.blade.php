@extends('layouts.master')

@section('content')

<div class="row">

    <div class="col-sm-4 border-right">
        <img src="{{ url('storage/clientes/'.$cliente->imagen)}}" width="600px" class="img-fluid"/>
    </div>
    <div class="col-sm-8">
        <h4 style="min-height:45px;margin:5px 0 10px 0">{{$cliente->nombre}}</h4>
        <p class="lead">Fecha de nacimiento: {{$cliente->fecha_nacimiento}}</p>
        <p class="lead">Correo: {{$cliente->correo}}</p>

        <div class="row">
            <div class="col-1 mr-2">        
                <a class="btn btn-primary" href="{{url('/catalog/edit/'.$cliente->id)}}" role="button">Editar</a>
            </div>
            <div class="col-1 mr-2"> 
                <form action="{{action('CatalogController@putDelete', $cliente->id)}}" method="POST" style="display:inline">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger" style="display:inline">
                        Borrar
                    </button>
                </form>
            </div>
            <div class="col">        
                <a class="btn btn-success" href="/catalog" role="button">Volver</a>
            </div>
        </div>
    </div>
</div>

@stop