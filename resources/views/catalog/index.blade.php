@extends('layouts.master')

@section('content')

<div class="row">

@foreach( $arrayClientes as $key => $cliente )
<div class="col-xs-6 col-sm-4 col-md-3 text-center mx-1">

    <a href="{{ url('/catalog/show/' . $cliente->id ) }}">
        <img src="{{ url('storage/clientes/'.$cliente->imagen)}}" width="200px" style="height:150px" class="img-fluid"/>
        <h4 style="min-height:45px;margin:5px 0 10px 0">
            {{$cliente->nombre}}
        </h4>
    </a>

</div>
@endforeach

</div>
@stop