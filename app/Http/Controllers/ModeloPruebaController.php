<?php

namespace App\Http\Controllers;

use App\ModelModeloPrueba;
use Illuminate\Http\Request;

class ModeloPruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelModeloPrueba  $modelModeloPrueba
     * @return \Illuminate\Http\Response
     */
    public function show(ModelModeloPrueba $modelModeloPrueba)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelModeloPrueba  $modelModeloPrueba
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelModeloPrueba $modelModeloPrueba)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelModeloPrueba  $modelModeloPrueba
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelModeloPrueba $modelModeloPrueba)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelModeloPrueba  $modelModeloPrueba
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelModeloPrueba $modelModeloPrueba)
    {
        //
    }
}
