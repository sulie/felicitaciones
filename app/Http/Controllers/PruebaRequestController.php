<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaRequestController extends Controller
{
    public function index (Request $request){
        //dd($request);
        echo $request->path()."<br>";
        echo $request->url()."<br>";
        echo $request->input('titulo')."<br>";
        echo $request->fullUrl()."<br>";

        return response('Respuesta', 200)->header('Content-Type', 'text/plain');
    }
}
