<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use Mail;

class MailController extends Controller
{
    public function basic_email($cliente) {
        $data = array('name'=>"Raul");
        Mail::send(['text'=>'mail'], $data, function($message) {
            $message->to($cliente->correo, $cliente->nombre)->subject
                ('Felicidades '.$cliente->nombre);
            $message->from('raulpruebascodigo@gmail.com','Felicitaciones');
            echo('Felicitacion enviada');
        });
    }

    public function html_email($cliente) {
        $data = array('name'=>"Raul");
        Mail::send('mail', $data, function($message) {
            $message->to($cliente->correo, $cliente->nombre)->subject
                ('Felicidades '.$cliente->nombre);
            $message->from('raulpruebascodigo@gmail.com','Felicitaciones');
            echo('Felicitacion enviada');
        });
    }

    public function attachment_email($cliente) {        
        $data = array('name'=>"Raul");
        Mail::send('mail', $data, function($message) {
            $message->to($cliente->correo, $cliente->nombre)->subject
                ('Felicidades '.$cliente->nombre);
            $message->attach('/home/vagrant/Laravel_Projects/felicitaciones/public/images/placeholder.png');
            $message->from('raulpruebascodigo@gmail.com','Felicitaciones');
            echo('Felicitacion enviada');
        });
    }
}
