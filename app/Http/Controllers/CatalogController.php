<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Cliente;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ClienteEditFormRequest;

class CatalogController extends Controller
{
    public function getIndex() {
        
        return view ('catalog/index', array('arrayClientes'=>Cliente::all()));
    }

    public function getShow($id) {
        return view('catalog/show', array('cliente'=>Cliente::findOrFail($id)));
    }

    public function getCreate() {
        return view('catalog/create');
    }

    public function postCreate(Request $request)
    {
        // EJERCICIO 2 VALIDATOR
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100',
            'fecha' => 'required|date|before_or_equal:today',
            'email' => 'required|email:rfc,dns'
        ]);

        if ($validator->fails()) {
            return redirect('catalog/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        if ($validator->fails()) {
            return redirect('post/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        // Creamos el objeto cliente
        $new_cliente = new Cliente();

        /* Bloque de asignación de datos */
        $new_cliente->nombre = $request->input('title');            // Asignacion de nombre
        $new_cliente->fecha_nacimiento = $request->input('fecha');  //Asignación de fecha de nacimiento
        $new_cliente->correo = $request->input('email');            //Asignación de correo
        /* FIN Bloque de asignación de datos */

        $new_cliente->save(); // Subida a la base de datos

        $new_id = Cliente::orderBy('id','desc')->first()->id;    //Obtención del ultimo id (objeto creado)

        /* Asignacion de la RUTA de la imagen */
        /* Bloque de subida a disco de archivos */
        if ($request->hasFile('foto')) {    //Se envia la foto
            if ($request->file('foto')->isValid()) {    //Se envia correctamente
                $ext = $request->file('foto')->extension(); //Obtener extensión de fichero
                $this_cliente = Cliente::find($new_id);     //Obtener el cliente insertado a partir del ultimo id
                $this_cliente->imagen = $new_id.'.'.$ext;   //Acualizar/Añadir en la base de datos el nombre del archivo  
                $request->file('foto')                      //Subida en el servidor (storage/public/clientes/id.ext)
                ->storeAs('clientes', $new_id.'.'.$ext, ['disk' => 'public']);     
                $this_cliente->save();                      // Subida a la base de datos post adición de foto (update + foto)
            }
        }
        
        /* FIN de bloque de subida de archivos a disco */
        /* FIN Asignacion de la RUTA de la imagen */

        return redirect()->action('CatalogController@getIndex');
    }

    public function getEdit($id) {
        return view('catalog/edit', array('cliente'=>Cliente::findOrFail($id)));
    }

    //EJERCICIO 3 VALIDADO CON REQUEST
    public function putEdit(ClienteEditFormRequest $request, $id)
    {
        // Obtenemos el cliente que queremos modificar
        $old_cliente = Cliente::find($id);
        $old_imagen = $old_cliente->imagen; //Y su actual foto

        /* Bloque de actualización de datos */
        $old_cliente->nombre = $request->input('title');    //Modificamos el nombre
        
        /* Bloque de actualización de imagen */
        if ($request->hasFile('foto')) {    //Se actualiza foto? ->
            if ($request->file('foto')->isValid()) {    //Es válida? ->
                $ext = $request->file('foto')->extension(); //Obtener extensión de fichero
                $old_cliente->imagen = $id.'.'.$ext;        //Actualizar campo imagen con su id y el cambio de extensión si lo hubiera
                Storage::disk('public')                     //En el disco public de storage...
                ->delete('clientes/'.$old_imagen);          //Borrar en la carpeta clientes la siguiente imagen
                $request->file('foto')                      //Subida en el servidor (storage/app/clientes/id.ext)
                ->storeAs('clientes', $id.'.'.$ext, ['disk' => 'public']);  
            }
        }
        /* FIN Bloque de actualización de imagen */
        $old_cliente->fecha_nacimiento = $request->input('fecha');
        $old_cliente->correo = $request->input('email');
        /* FIN Bloque de actualización de datos */

        $old_cliente->save(); // Subida a la base de datos
        return redirect()->action('CatalogController@getShow', ['id' => $id]);
    }

    public function putDelete($id) {
        $cliente = Cliente::find($id);
        Storage::disk('public')                     //En el disco public de storage...
        ->delete('clientes/'.$cliente->imagen);     //Borrar en la carpeta clientes la siguiente imagen
        $cliente->delete();
        return redirect()->action('CatalogController@getIndex');
    }
}
