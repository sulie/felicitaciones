<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteEditFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'fecha' => 'required|date|before_or_equal:today',
            'email' => 'required|email:rfc,dns'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El nombre es obligatorio',
            'title.max' => 'El nombre debe contener menos de 100 caracteres',
            'fecha.required'  => 'La fecha es obligatorio',
            'fecha.date' => 'Introduce una fecha válida',
            'fecha.before_or_equal:today' => 'La fecha debe ser anterior a la de hoy',
            'email.required' => 'El email es obligatorio',
            'email.email' => 'Introduce un email válido'
        ];
    }
}
