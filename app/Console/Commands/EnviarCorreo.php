<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Cliente;

class EnviarCorreo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando comprueba a quien hay que enviar un correo de felicitación';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clientes = Cliente::all();
        foreach($cliente as $cliente) {
            if (date('m-d',$cliente->fecha_nacimiento) == date('m-d')) {
                app('App\Http\Controllers\MailController')->basic_email($cliente);
            }
        }
    }
}
